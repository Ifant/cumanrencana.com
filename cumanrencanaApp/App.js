import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import * as firebase from 'firebase';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer} from '@react-navigation/native';
import { StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons } from '@expo/vector-icons';

import Login from './src/Login';
import Register from './src/Register';
import Home from './src/Home';
import Loading from './src/loading';
import Profil from './src/Profil';
import WriteScreen from './src/Write';
import EditProfil from './src/Profil/editProfil';

const firebaseConfig = {
  apiKey: "AIzaSyDodC1jq2wcksbNv9PTwNpGqxWx9Dhk308",
  authDomain: "cumanrencana.firebaseapp.com",
  databaseURL: "https://cumanrencana.firebaseio.com",
  projectId: "cumanrencana",
  storageBucket: "cumanrencana.appspot.com",
  messagingSenderId: "655160707731",
  appId: "1:655160707731:web:20a8ba7dec274177a35b66",
  measurementId: "G-1DXL7ZLM9V"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function App() {
  const [statUser,setStatUser] = React.useState(null);

  firebase.auth().onAuthStateChanged(user => {
    user ? (setStatUser(true)) : (setStatUser(false));
  })

  if (statUser){
    return (
      <NavigationContainer>
        <Tab.Navigator 
        screenOptions={({route}) => ({
          tabBarIcon: ({color}) => {
            if (route.name==='Home'){
              return(
                <MaterialIcons name='home' size={32} color={color}/>
              )
            }
            else if (route.name === 'Profil'){
              return(
                <MaterialIcons name='person' size={32} color={color}/>
              )
            }
            else if (route.name==='Write'){
              return(
                <MaterialIcons name='library-books' size={32} color={color}/>
              )
            }
          }
        })}
        tabBarOptions= {{
          activeTintColor: '#EB3B5A',
          showLabel: false
        }}
        initialRouteName='Home'>
          <Tab.Screen name='Home' component={Home} ></Tab.Screen>
          <Tab.Screen name='Write' component={WriteScreen}/>
          <Tab.Screen name='Profil' component={Profil}></Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>
    );
  }else{
    return(
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Login'>
          <Stack.Screen name='Login' component={Login} options={{headerShown:false}}></Stack.Screen>
          <Stack.Screen name='Register' component={Register} options={{headerShown:false}} ></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
  // return(
  //   <EditProfil/>
  //   // <WriteScreen/>
  //   // <Profil></Profil>
  // )
}

function SettingsScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center',backgroundColor:'white' }}>
      <Text>Settings!</Text>
    </View>
  );
}

function Write() {
  return(
    <View style={{flex:1,justifyContent:"center",alignItems:'center',backgroundColor:'white'}}>
      <Text>Write Plan Screen</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
