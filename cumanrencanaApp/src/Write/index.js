import React, { Component } from 'react';
import { View,Text,TouchableOpacity,StyleSheet } from 'react-native';

import * as firebase from 'firebase';

export default class Write extends Component {
    render() {
        return (
            <View style= {styles.container}>
                <View style = {styles.header}>
                    <Text>TULIS RENCANA</Text>
                </View>
                <Text style = {styles.title}>Tulis Rencana Mu Untuk {'\n'}
                Masa Depan Bangsa</Text>
                <Form nama = 'Nama App'/>
                <Form nama = 'Signifikansi Masalah'/>
                <Form nama = 'Pengenalan Solusi'/>
                <Form nama = 'Dampak Solusi'/>
                <Form nama = 'Kesimpulan'/>
                <TouchableOpacity>
                    <View style ={styles.buttonShare}>
                        <Text style = {styles.shareText}>SHARE</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const Form = (props) => (
    <TouchableOpacity>
        <Text style ={ styles.name}>{props.nama}</Text>
        <View style = {styles.garis}></View>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        height: 49,
        backgroundColor: '#EB3B5A',
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 19,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 26,
        marginBottom: 30
    },
    name : {
        fontSize: 15,
        marginLeft: 20,
        marginTop: 10
    },
    garis: {
        height: 1,
        backgroundColor: 'black',
        marginTop: 33,
        marginLeft: 20,
        marginRight: 20
    },
    buttonShare: {
        height: 30,
        backgroundColor: '#EB3B5A',
        borderRadius: 9,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    shareText: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    }
})