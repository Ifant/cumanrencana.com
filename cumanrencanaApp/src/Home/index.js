import React, { Component } from 'react';
import { View,Text,TouchableOpacity,Image,StyleSheet,ScrollView,StatusBar,Button,FlatList } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import * as firebase from 'firebase';

const DATAS = [
    {
        id: '1',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '2',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '3',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '4',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '5',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '6',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '7',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '8',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '9',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '10',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '11',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '12',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    }
]

const Item = ({ judul,perencana,tanggal,rencana }) => (
    <View style={styles.item}>
        <View style={styles.garis}></View>
        <TouchableOpacity onPress={() => alert('Ini Pencetan Rencana')}>
            <View style={{flexDirection:"row",marginTop: 14, marginBottom:14,marginLeft: 15,marginRight:130}}>
                <View style={styles.rencanaPicture}></View>
                <View>
                    <Text style={styles.judul}>{judul}</Text>
                    <Text style={styles.perencanaTanggal}>{perencana}</Text>
                    <Text style={styles.perencanaTanggal}>{tanggal}</Text>
                    <Text style={styles.rencana}>{rencana}</Text>
                </View>
            </View>
        </TouchableOpacity>
    </View>
);

export default class Home extends Component {
    state = {
        displayName:'',
        email:''
    }
    logOutHandler(){
        firebase.auth().signOut()
    }
    componentDidMount(){
        const { email,displayName } = firebase.auth().currentUser;
        this.setState({displayName});
        this.setState({email});
    }
    render() {
        return(
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ED4C67' barStyle='light-content' />
                <View style={styles.header}>
                    <Text style={styles.headerText}>cumanRencana.com</Text>
                </View>
                <Text style={styles.helloText}>Helo {this.state.email}! {'\n'}Have a Nice Day!</Text>
                <View style = {styles.buttonOut}>
                    <Button
                    title='LOG OUT'
                    color='#e74c3c'
                    onPress= {() => this.logOutHandler()}
                    >
                    </Button>
                </View>
                <FlatList
                data = {DATAS}
                renderItem = {({item}) => (
                    <Item
                    perencana = {item.perencana}
                    judul = {item.judul}
                    tanggal = {item.tanggal}
                    rencana = {item.rencana}
                    />
                )}
                keyExtractor = {item => item.id}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
         flex: 1,
         backgroundColor: 'white'
    },
    header: {
        height: 49,
        backgroundColor: '#EB3B5A',
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 25,
        color: 'white'
    },
    helloText: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 30
    },
    footer: {
        backgroundColor: '#EB3B5A',
        borderTopStartRadius: 2,
        borderTopRightRadius: 2,
        height: 42,
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    buttonOut: {
        width: 135,
        marginLeft: 'auto',
        marginRight: 'auto',
        top: 10,
        marginBottom: 20
    },
    garis: {
        height: 1,
        backgroundColor: '#878686',
        marginLeft: 15,
        marginRight: 15,
    },
    rencanaPicture:{
        width: 100,
        height: 100,
        backgroundColor: '#EB3B5A',
        marginRight: 10
    },
    perencanaTanggal:{
        fontSize: 11
    },
    judul: {
        fontWeight: 'bold',
        fontSize: 15,
    }
})