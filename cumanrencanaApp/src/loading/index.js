import React, { Component } from 'react';
import { View,ActivityIndicator,Text,StyleSheet,Image,StatusBar } from 'react-native';
import * as firebase from 'firebase';

export default class Loading extends Component {
    componentDidMount(){
        firebase.auth().onAuthStateChanged(user => {
            this.props.navigation.navigate(user ? 'Home':'Login');
        })
    }
    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ED4C67' barStyle='light-content' />
                <Text style={styles.loadingText}>Loading...</Text>
                <ActivityIndicator size='large' color='white'></ActivityIndicator>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor: '#EB3B5A'
    },
    loadingText: {
        fontSize: 15,
        color: 'white'
    }
})