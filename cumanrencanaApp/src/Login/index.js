import React, { Component } from 'react';
import { StatusBar,StyleSheet,View,Text,Image,TextInput,TouchableOpacity } from 'react-native';
import * as firebase from 'firebase';

export default class Login extends Component {
    state = {
        email: '',
        password: '',
        errorMessage: ''
    }
    loginHandler(){
        firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email,this.state.password)
        .then(
            () => this.setState({email:'',password:'',errorMessage:''}))
        .catch(
            error => this.setState({errorMessage: error.message}));
        
    }
    render(){
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ED4C67' barStyle='light-content' />
                <Image style={styles.logo} source={require('../image/think.jpg')}></Image>
                <Text style= {styles.logoText}>cumanRencana.com</Text>
                <Text style={styles.loginText}>LOGIN</Text>
                <TextInput 
                style= {styles.form}
                placeholder='Email'
                onChangeText = {email => this.setState({email})}
                value = {this.state.email}
                ></TextInput>
                <TextInput
                style={styles.form}
                placeholder='Password'
                onChangeText = {password => this.setState({password})}
                value = {this.state.password}
                secureTextEntry
                autoCapitalize = 'none'
                ></TextInput>
                <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
                <View style={{justifyContent: 'center',alignItems:'center',marginTop:10}}>
                    <TouchableOpacity onPress={()=> this.loginHandler()}>
                        <View style= {styles.formSubmitLogin}>
                            <Text style= {styles.formSubmitText}>LOGIN</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity>
                        <Text style={{color:'#EB3B5A'}}>Forgot Password?</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('Register')}>
                        <View style= {styles.formSubmitRegister}>
                            <Text style= {styles.formSubmitText}>Create New Account</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    logo: {
        width: 172,
        height: 165,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 30
    },
    logoText: {
        fontSize: 20,
        textAlign: 'center',
    },
    loginText: {
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    form: {
        width: 235,
        height: 45,
        borderColor: '#EB3B5A',
        borderWidth: 2,
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 7,
        marginBottom: 2.36,
        padding: 10
    },
    formSubmitLogin: {
        width: 235,
        height: 34,
        backgroundColor: '#EB3B5A',
        borderRadius: 9,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom:7
    },
    formSubmitRegister: {
        width: 150,
        height: 34,
        backgroundColor: '#34495e',
        borderRadius: 9,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    formSubmitText: {
        fontWeight: 'bold',
        color: 'white',
    },
    errorMessage: {
        fontSize: 14,
        textAlign: 'center',
        top: 5,
        color:'red'
    }

})