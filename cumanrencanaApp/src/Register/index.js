import React, { Component } from 'react';
import { StatusBar,StyleSheet,View,Text,Image,TextInput,TouchableOpacity } from 'react-native';
import * as firebase from 'firebase';

export default class Login extends Component {
    state = {
        displayName:'',
        email: '',
        password: '',
        confirmPassword: '',
        errorMessage: ''
    }
    registerHandler(){
        if (this.state.password === this.state.confirmPassword){
            firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email,this.state.password)
            .then(userCredential => {
                return userCredential.user.updateProfile({displayName: this.state.displayName});
            })
            .catch(error => this.setState({errorMessage: error.message}));
        }
        else {
            this.setState({errorMessage: 'Password missmatch'})
        }
    }
    render(){
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor= '#ED4C67' barStyle='light-content' />
                <Image style={styles.logo} source={require('../image/think.jpg')}></Image>
                <Text style= {styles.logoText}>cumanRencana.com</Text>
                <Text style={styles.loginText}>REGISTER</Text>
                <TextInput 
                style= {styles.form}
                placeholder='Full Name'
                onChangeText = {displayName => this.setState({displayName})}
                value={this.state.displayName}
                ></TextInput>
                <TextInput 
                style= {styles.form}
                placeholder='Email'
                onChangeText = {email => this.setState({email})}
                value={this.state.email}
                ></TextInput>
                <TextInput
                style={styles.form}
                placeholder='Password'
                onChangeText = {password => this.setState({password})}
                value = {this.state.password}
                secureTextEntry
                autoCapitalize='none'
                ></TextInput>
                <TextInput
                style={styles.form}
                placeholder='Confirm Password'
                onChangeText = {confirmPassword => this.setState({confirmPassword})}
                value = {this.state.confirmPassword}
                secureTextEntry
                autoCapitalize='none'
                ></TextInput>
                <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
                <TouchableOpacity onPress={() => this.registerHandler()}>
                    <View style= {styles.formSubmit}>
                        <Text style= {styles.formSubmitText}>REGISTER</Text>
                    </View>
                </TouchableOpacity>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: "center",
        alignItems: 'center'
    },
    logo: {
        width: 172,
        height: 165,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    logoText: {
        fontSize: 20,
        textAlign: 'center',
    },
    loginText: {
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10,
        marginBottom: 10
    },
    form: {
        width: 235,
        height: 45,
        borderColor: '#EB3B5A',
        borderWidth: 2,
        borderRadius: 7,
        marginBottom: 2.36,
        padding: 10
    },
    formSubmit: {
        width: 235,
        height: 34,
        backgroundColor: '#EB3B5A',
        borderRadius: 9,
        alignItems: 'center',
        justifyContent: 'center',
    },
    formSubmitText: {
        fontWeight: 'bold',
        color: 'white',  
    },
    errorMessage: {
        fontSize: 14,
        textAlign: 'center',
        top: 5,
        color: 'red',
        marginBottom: 10
    }

})