import React, { Component } from 'react';
import { View,StyleSheet,Text,TouchableOpacity,StatusBar } from 'react-native';
import * as firebase from 'firebase';
import { Feather,MaterialIcons } from '@expo/vector-icons';

export default class WriteLanj extends Component { 
    render(){
        return(
            <View style = {styles.container}>
                <View style ={styles.header}>
                    <TouchableOpacity>
                     <Feather name = 'x' size={30} color='white' style ={{marginLeft: 10}}/>
                    </TouchableOpacity>
                    <Text>Edit Profil</Text>
                    <TouchableOpacity>
                        <View style ={styles.buttonSave}>
                            <Text style={styles.saveText}>SAVE</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.backProfilePicture}>
                    <MaterialIcons name = 'person' size = {70} color = 'white'/>
                </View>
                <TouchableOpacity>
                    <Text style ={styles.changeProfileText}>Change Profile Picture</Text>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Form nama = 'Name'/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Form nama = 'Email'/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Form nama = 'Bio'/>
                </TouchableOpacity>
            </View>
        )
    }
}

const Form = (props) => (
    <View>
        <Text style = {styles.name}>{props.nama}</Text>
        <View style = {styles.garis}/>
    </View>
)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 49,
        backgroundColor: '#EB3B5A',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    buttonSave: {
        width: 64,
        height: 22,
        borderRadius: 9,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10
    },
    backProfilePicture: {
        width: 79,
        height: 79,
        backgroundColor: '#EB3B5A',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 79/2,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 31
    },
    changeProfileText: {
      textAlign: 'center',
      fontSize: 16,
      fontWeight: 'bold',
      color: '#0148FF',
      marginTop: 14
    },
    name : {
        fontSize: 15,
        marginLeft: 34,
        marginTop: 10
    },
    garis: {
        width: 345,
        height: 1,
        backgroundColor: 'black',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 33
    },

})