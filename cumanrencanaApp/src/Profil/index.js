import React, { Component } from 'react';
import { View,Text,Image,StyleSheet,StatusBar,FlatList,SafeAreaView,TouchableOpacity,ScrollView } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import * as firebase from 'firebase';
import { State } from 'react-native-gesture-handler';

const DATAS = [
    {
        id: '1',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '2',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    },
    {
        id: '3',
        judul: 'Sodaqoh Online',
        perencana: 'Alex Alexander',
        tanggal: '13/04/2020',
        rencana: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ',
    }
]

const Item = ({ judul,perencana,tanggal,rencana }) => (
    <ScrollView style={styles.item}>
        <View style={styles.garis}></View>
        <TouchableOpacity onPress={() => alert('Ini Pencetan Rencana')}>
            <View style={{flexDirection:"row",marginTop: 14, marginBottom:14,marginLeft: 15,marginRight:130}}>
                <View style={styles.rencanaPicture}></View>
                <View>
                    <Text style={styles.judul}>{judul}</Text>
                    <Text style={styles.perencanaTanggal}>{perencana}</Text>
                    <Text style={styles.perencanaTanggal}>{tanggal}</Text>
                    <Text style={styles.rencana}>{rencana}</Text>
                </View>
            </View>
        </TouchableOpacity>
    </ScrollView>
);
  

export default class Profil extends Component{
    state = {
        displayName: '',
    }
    componentDidMount(){
        const { email,displayName } = firebase.auth().currentUser;
        this.setState({displayName});
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <Text style={styles.headerText}>MY PROFIL</Text>
                </View>
                <View style={styles.backgroundProfilePicture}>
                    <MaterialIcons name='person' size={100} color='white'/>
                </View>
                <Text style={styles.name}>{this.state.displayName}</Text>
                <Text style={styles.bio}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
                    nostrud </Text>
                <Text style={styles.textRencana}>Rencana</Text>
                <Text style={styles.jumlahRencana}>3</Text>
                <TouchableOpacity onPress={() => alert('Ini Pencetan Edit Profil!')}>
                    <View style={styles.editProfilButton}>
                        <Text style={styles.editProfilText}>Edit Profil</Text>
                    </View>
                </TouchableOpacity>
                <FlatList
                data = {DATAS}
                renderItem = {({item}) => (
                    <Item
                    perencana = {item.perencana}
                    judul = {item.judul}
                    tanggal = {item.tanggal}
                    rencana = {item.rencana}
                    />
                )}
                keyExtractor = {item => item.id}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 49,
        backgroundColor: '#EB3B5A',
        alignItems: 'center',
        justifyContent: 'center'
    },
    backgroundProfilePicture: {
        width: 127,
        height: 127,
        borderRadius: 127/2,
        backgroundColor: '#EB3B5A',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    name: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 22
    },
    bio: {
        textAlign: 'center',
        fontSize:12,
        marginLeft:56,
        marginRight:56,
        marginTop: 8
    },
    textRencana: {
        fontSize: 20,
        textAlign:'center',
        marginTop: 10
    },
    jumlahRencana: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    editProfilButton: {
        width: 299,
        height:25,
        backgroundColor: '#EB3B5A',
        borderRadius: 4,
        alignItems: "center",
        marginRight: 'auto',
        marginLeft: 'auto',
        marginTop: 5,
        marginBottom: 10
    },
    editProfilText: {
        fontSize: 16,
        color: 'white',

    },
    garis: {
        height: 1,
        backgroundColor: '#878686',
        marginLeft: 15,
        marginRight: 15,
    },
    rencanaPicture:{
        width: 100,
        height: 100,
        backgroundColor: '#EB3B5A',
        marginRight: 10
    },
    perencanaTanggal:{
        fontSize: 11
    },
    judul: {
        fontWeight: 'bold',
        fontSize: 15,
    }
})